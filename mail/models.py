from django.db import models


# Create your models here.
class Mail(models.Model):
    #id = models.AutoField(primary_key = True)
    email = models.EmailField()
    title = models.CharField(max_length=50)
    content = models.TextField()

    def __str__(self):
        return self.email + ', ' + self.title

