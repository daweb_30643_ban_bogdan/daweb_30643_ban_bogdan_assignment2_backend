from rest_framework import serializers
from mail.models import Mail


class MailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mail
        fields = ("id","email","title","content")