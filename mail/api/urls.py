from django.urls import path
from .views import MailUpdateView,MailCreateView,MailDeleteView,MailListView,MailDetailsView

urlpatterns = [
    path('',MailListView.as_view()),
    path('<pk>',MailDetailsView.as_view()),
    path('create/',MailCreateView.as_view()),
    path('delete/<pk>',MailDeleteView.as_view()),
    path('update/<pk>',MailUpdateView.as_view())
]