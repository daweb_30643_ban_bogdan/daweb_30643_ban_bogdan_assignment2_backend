from rest_framework.generics import \
    ListAPIView,\
    RetrieveAPIView,\
    CreateAPIView,\
    DestroyAPIView,\
    UpdateAPIView

from mail.api.serializers import MailSerializer
from mail.mail_sender import MailSender
from mail.models import Mail


class MailListView(ListAPIView):
    queryset = Mail.objects.all()
    serializer_class = MailSerializer


class MailDetailsView(RetrieveAPIView):
    queryset = Mail.objects.all()
    serializer_class = MailSerializer


class MailCreateView(CreateAPIView):
    queryset = Mail.objects.all()
    serializer_class = MailSerializer

    def create(self, request, *args, **kwargs):
        print(self.request.data['email'])
        print(self.request.data['title'])
        print(self.request.data['content'])
        m = MailSender(self.request.data['email'],self.request.data['title'],self.request.data['content'])
        m.send()
        return super(MailCreateView,self).create(request,*args,**kwargs)


class MailDeleteView(DestroyAPIView):
    queryset = Mail.objects.all()
    serializer_class = MailSerializer


class MailUpdateView(UpdateAPIView):
    queryset = Mail.objects.all()
    serializer_class = MailSerializer

